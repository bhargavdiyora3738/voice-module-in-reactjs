import React, { useState, useEffect } from 'react';
import './App.css';


const SpeechRecognition =
  window.SpeechRecognition || window.webkitSpeechRecognition
const mic = new SpeechRecognition()

mic.continuous = true
mic.interimResults = true
mic.lang = 'en-US'

function App() {
  const [isListening, setIsListening] = useState(true)
  const [note, setNote] = useState(null)


  useEffect(() => {
    handleListen()
  }, [isListening])

  const handleListen = () => {
    if (isListening) {
      mic.start()
      mic.onend = () => {
        console.log('continue..')
        mic.start()
      }
    } else {
      mic.stop()
      mic.onend = () => {
        console.log('Stopped Mic on Click')
      }
    }
    mic.onstart = () => {
      console.log('Mics on')
    }

    mic.onresult = event => {
      const transcript = Array.from(event.results)
        .map(result => result[0])
        .map(result => result.transcript)
        .join('')
      console.log(transcript)
      setNote(transcript)
      mic.onerror = event => {
        console.log(event.error)
      }
    }
  }

  

  return (
    
      
      
    <div>
      {isListening?<div><div className="center">
         <div className="outer button">
          
          <button onClick={() => setIsListening(prevState => !prevState)} > </button>
          
         
         </div>
      </div>
      </div> : <div>
          <div className="center">
         
        <button className="cbutton" onClick={() => setIsListening(prevState=>!prevState)}  >
          </button></div></div>}
      <div className="Box">
        <p>{note}</p>
      </div>
        
    </div>
    
        
    
  )
}

export default App
